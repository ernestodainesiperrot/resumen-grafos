# Grafos

Matemática Discreta, Facultad de Ingeniería Universidad de Buenos Aires  

Resumen de teoría de grafos. La mayoría del contenido de este resumen fue sacado
de las clases teoricas de Fernando Acero.

## Definiciones básicas de un Grafo

Generalmente a los grafos de lo definen con la letra $G$.  
Los grafos estan formados por un conjunto de:  

- Vertices $\to$ V
- Aristas $\to$ A (o E, por su definición en inglés *Edge*)  

Al conjunto de estos vertices se lo denota $V(G)$, y al conjunto
de las aristas $E(G)$

Cantidad de vertices = orden del grafo = $|V(G)|$ = $n$  
Cantidad de aristas = tamaño del grafo = $|E(G)|$ = $m$  

![grafo](./images/grafo1.png)
> Este grafo $G$, es simple

$V(G)= \{v_1, v_2, v_3, v_4, v_5\}$, $n = |V(G)| = 5$  
$E(G)= \{e_1, e_2, e_3, e_4, e_5, e_6\}$, $m = |E(G)| = 6$

### Definición propia de un grafo

$G = (V(G), E(G), \psi)$ siendo $\psi$ la función que asigna aristas a pares
(no ordenados) de vértices (o a un vértice).

### Grafo Simple

Un grafo es **simple** si $\psi$ es **inyectiva**, y además carece de **lazos**[^1].
[^1]: Un lazo, también llamado bucle o *loop*, es una arista que sale y entra en un mismo vértice.

Para los grafos simples, vale designar las aristas por el par de vértices que une.

![grafo-no-simple](./images/grafo-con-loop.png)
> Este grafo no es simple ya que contiene un *loop*

### Grado de un vértice

El grado de un vertice $v$, se escribe como $d(v)$ [*degree*],
es la cantidad de aristas que incide sobre $v$
> Cuando se cuenta el grado de un vértice, los lazos, cuentan doble.

Y los *nodos* de grado 1 se llaman colgantes (hojas[^2]).
[^2]: Generalmete se los llama así en árboles.

La *sucesión de grados* $d(G)$, ordena en un vector los grados de los nodos
de manera no decreciente.
También se los puede ordenar de manera decreciente, pero se suele optar por alguna.

![grado-de-un-vertice](./images/grados-de-un-vertice.png)

$G = (V(G), E(G)), n(G) = 5, m(G) = 8$  
$d(G) = (1, 3, 4, 4, 4)$  

**Observación**: $1 + 3 + 4 + 4 + 4 = 16$ es el doble del tamaño.
Esto es lo que dice el **HSL** (*Hand Shaking Lemma*), la suma de grados de $G$ duplica
su tamaño.  
La pruba de esto:  
Toda arista incide sobre dos vértices (y cuando es un lazo incide dos
veces sobre el mismo vértice)
, luego cada arista contribuye en 2 a la suma de grados.  

**Consecuencia:**

1. La suma de los grados es par, $\sum^{n}_{k=1} d(v_k) = 2m$
2. La cantidad de vértices de grado impar debe ser par.

## Grafos "Famosos"

### Grafo simple **saturado de aristas[^3]** (completo $K_n$)  

$K_n$ es la palabra reservada para los grafos completos

![grafo-k6](./images/grafo-completo-k6.png)
> Grafo $K_6$

¿Cual es la máxima cantidad de aristas que puede tener un grafo simple?  
$m(K_n) = {^1/_2} (n (n-1))$  
$d(K_n) = (n - 1, n - 1, ..., n - 1)$
[^3]: máxima cantidad de aristas que le puedo poner a $G$, sin perder la simplicidad.

### Grafo nulo $N_n$

Es el grafo de $n$ vértices sin aristas.  

![grafo-nulo-4](./images/grafo-nulo.png)
> Grafo $N_4$

$d(N_n) = (0, 0, ..., 0)$

### El grafo $P_n$ (Path)

Es un grafo "lineal" de una sucesión de vértices.  
![path-graph](./images/path-graph.png)
> Grafo $P_4$

$d(P_n) = (1, 1, 2, 2, 2, ..., 2), n \ge 2$

### El ciclo $C_n$

> Pensarlo como un Path con los extremos unidos

*Agregar imagenes de ejemplo*  
$d(C_n) = (2, 2, ..., 2)$

### Completos Bipartitos $K_{p, q}$

$V(K_{p, q}) = V_1 + V_2$ con $V_1V_2 = \empty$, saturados de aristas *pero* los
vértices adyacentes **no pertenecen al mismo conjunto** (un vértice de $V_1$ con
uno de $V_2$). [$|V_1| = p, |V_2| = q$].  
*Agregar imagenes de ejemplo*  
$m(K_{p, q}) = pq$

### Bipartitos

*En estos no tenemos la exigencia de la saturación.*  
(Agregar imagen)  
> Observación: los ciclos de orden par son bipartitos (no los son,
> los de orden impar) *Agregar imagenes de ejemplo*

#### La estrella

Es otro grafo bipartito  
*Agregar imagen*

## Grafo Regular

Un grafo es regular si:  
$\forall \space v \space \in \space V(G): d(v) = K (es \space "k-regular")$  
*Para todo vertice, de su conjunto de vertices, el grado es el mínimo*
> Si un grafo es k-regular, $m(G) = kn$

## Complemento de un grafo

> Solo está definido para grafos simples

Notación: $G'$  
**Definición**  
$G = (V(G), E(G)), G' = (V(G'), E(G'))$ donde $V(G') = V(G), e \space \in
\space E(G') \Leftrightarrow e \notin \space E(G)$ [$G$ es simple]  

> Definición: grafo *auto complementario*, su complemento es el mismo.

## Grafos Isomorfos

**Definición:**  

- Una función $f: V(G) \to V(H)$ biyectiva tal que $e = xy \space
\in \space E(G) \space \Leftrightarrow \space f(x)f(y) \space \in \space E(H)$
es un *isomorfismo* entre $G$ y $H$. Los grafos $G$ y $H$ son ismorfos
$G \cong H$. Dicho brevemente, "preserva aristas".  

Si dos vértices en un grafo están conectados por una arista,
las imagenes de esos vértices están conectados por la imagen de
esa arista.  
Los grafos isomorfos deben preservar sus rasgos estructurales.

## Camino o *Walk*

Un camino entre dos vértices es una sucesión de
vértices-aristas.  
Un **camino simple** es un camino que **no repite vértices**. También se lo llama
*Path*.

### Distancia entre dos vértices

La distancia se define por la cantidad (minima) de aristas
que se necesitan pasar por para llegar de un vértice $v$ a un $w$.  
Se lo denota $d(v, w)$

### Excentricidad

La excentricidad de un vértice es la distancia máxima de un vértice
con respecto al resto (siempre por el camino mmas corto).  

**Definición**:  

- $e(v) = max(d(v, x)), x \in V(G)$

#### Otras definiciones que se desprenden de la excentricidad

**Radio:** mínima excentricidad, $r(G)$  
**Diámetro:** máxima excentricidad, $\phi(G)$  
**Centro:** Conjunto de vértices que su excentricidad es mínima, $C(G)$  
**Periferia:** Conjunto de vértices que su excentricidad es máxima, $P(G)$  

## Grafo Conexo

Un grafo se llama conexo si existe un camino simple entre dos
cualesquiera de sus vértices.  

Observaciónes:  

- Si un grafo es no conexo, se divide en componentes conexas.
- La distancia entre dos vértices de componentes conexas distintas es $\infty$
